<!DOCTYPE html>
<html>
<head>
<title>Media Player Gambar dan Video Youtube</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="text/html; charset=iso-8859-2" http-equiv="Content-Type">
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script src="https://www.youtube.com/iframe_api"></script>
<style>
    .slides {
        display:none;
    }
    .media-container{
        max-width:500px;
        margin:0 auto;
    }
    .imgmedia{
        width:100%;
    }
</style>
</head>

<body>
    <div class="media-container">
        <img class="slides imgmedia" src="https://corona.kepriprov.go.id/resources/image/infografis/IMG-20200406-WA0025.jpg">
        <img class="slides imgmedia" src="https://corona.kepriprov.go.id/resources/image/infografis/IMG-20200401-WA0052.jpg">
        <a href="https://www.youtube.com/embed/_3xUtKVsDOw?list=PL9wC1lzonKjWSM-MitecggWVls8KFBBTC" class="slides"></a>
        <img class="slides imgmedia" src="https://corona.kepriprov.go.id/resources/image/infografis/PROVINSI_KEPRI_3.jpg">
        <a href="https://www.youtube.com/embed/89kK34vH3u0?list=PL9wC1lzonKjWSM-MitecggWVls8KFBBTC" class="slides"></a>
        <img class="slides imgmedia" src="https://corona.kepriprov.go.id/resources/image/infografis/etika_batuk_bersin.jpg"/>
        <div id="player" style='display:none'></div>
    </div>

    <script>
        let index = 0
        let player
        let time_update_interval
        let change_time = 3000
        slider();

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                width: 500,
                height: 490,
                events: {
                    onStateChange:updateTime
                }
            });
        }

        function updateTime(){
            clearInterval(time_update_interval);
            time_update_interval = setInterval(function () {
                if(player.getDuration() == player.getCurrentTime() && player.getCurrentTime() > 0){
                    clearInterval(time_update_interval)
                    $('#player').fadeOut(500)
                    setTimeout(slider, 800)
                }
            }, 1000)
        }

        function slider() {
            let i
            let x =  $('.slides')
            $('.slides').hide()
            
            index++;
            if (index > x.length)
                index = 1

            x.eq(index-1).fadeIn(1000)
            if(x.eq(index-1)[0].nodeName == 'IMG')
                setTimeout(slider, change_time)
            else{
                $('#player').fadeIn(1000)
                player.loadVideoByUrl(x.eq([index-1]).attr('href'))
                setTimeout(function(){
                    player.playVideo()
                    console.log('play video')
                },1000)
            }    
        }
    </script>
</body>
</html>
